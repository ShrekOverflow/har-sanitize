import whitelist from './whitelist';

export default function sanitize (harText) {
    const har = JSON.parse(harText);
    // This might corrupt the har though.
    har.log.entries = har.log.entries.filter(({request}) => {
        return whitelist.some(pattern => request.url.match(pattern));
    });
    return JSON.stringify(har);
}
