/**
 * Application entry point
 */

// Load application styles
import {saveAs} from 'file-saver';
import 'styles/index.scss';
import $ from 'jquery';
import sanitize from '../lib/sanitize';
// ================================
// START YOUR APP HERE
// ================================

$('#file').on('change', function (e) {
    const file = e.target.files[0];
    if(!file){
        alert('Error you must add a har file!');
    }
    const fr = new FileReader(file);
    fr.onload = function (e) {
        try{
            const harText = e.target.result;
            const sanitizedHarText  = sanitize(harText);
            const sanitizedFile = new File([sanitizedHarText], 'sanitized' + file.name, {
                type: "application/json;charset=utf-8"
            });
            saveAs(sanitizedFile);
        }catch(e){
            console.log(e);
            alert('Error process your file, are you sure its a har?')
        }
    }

    fr.onerror = function (e) {
        alert('Error loading file');
    }

    fr.readAsText(file, 'utf8');
});
